import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class sumOfTwoNumbers{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i<Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem(BufferedReader in) throws IOException {
		String line;
		int solution = 0;
		line = in.readLine();
		String[] result = line.split(" ");
		solution += Integer.parseInt(result[0]);
		solution += Integer.parseInt(result[1]);
		System.out.println(solution);

	}
}