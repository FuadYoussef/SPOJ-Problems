import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");




public class Prime1{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String	amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			if(i > 0){
				System.out.println();
			}
			doOneProblem(in);
		}
		
	}
	static void doOneProblem(BufferedReader in) throws IOException {
		String line;
		line = in.readLine();
		ArrayList<Integer> Primes = new ArrayList<>();
		String[] result = line.split(" ");
		for(int i = Integer.parseInt(result[0]); i <= Integer.parseInt(result[1]); i++){
			if(isPrime(i)){
				Primes.add(i);
			}
		}
		for(int i = 0; i < Primes.size(); i++){
			System.out.println(Primes.get(i));
		}
		
	}
	static boolean isPrime(int x){
		if(x < 2){
			return false;
		}
		for(int i = 2; i <= Math.sqrt(x); i++){
			if(x%i == 0){
				return false;
			}
		}
		return true;
	}
}
