import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class Sum {

	public static void main(String[] args) throws IOException {

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int total=0;
		for(int i = 0; i<2; i++){
			String line;
			line = in.readLine();
			if(line== null|| line.equals("")){
				break;
			}
			total += Integer.parseInt(line);
		}
		System.out.println(total);
	}
}
