import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class theNextPalindrome{

	public static void main(String[] args) throws IOException {
				
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while (true) {
			line = in.readLine();
			if (line == null || line.equals("42")) {
				break;
			}
			System.out.println(line);
		}
	}
}