import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");


//cant seem to simplify code correctly on this one

public class characterPatterns3{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}

	static void doOneProblem(BufferedReader in) throws IOException {
		String input;
		input = in.readLine();
		String[] result = input.split(" ");
		for(int i = 0; i < Integer.parseInt(result[0]); i++){
			String line = "";
			for(int j= 0; j<Integer.parseInt(result[1]); j++){
				if(i%2 ==0){
					if(j%2 == 0){
						line+= '*';
					}else{
						line+='.';
					}
					}else if(j%2 ==0){
						line += '.';
					}else{
						line +='*';
					}
				}
				System.out.println(line);
			}
		}
	}