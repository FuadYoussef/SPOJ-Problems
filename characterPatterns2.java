import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class characterPatterns2{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}

	static void doOneProblem(BufferedReader in) throws IOException {
		String input;
		input = in.readLine();
		String[] result = input.split(" ");
		for(int i = 0; i < Integer.parseInt(result[0]); i++){
			String line = "";
			for(int j= 0; j<Integer.parseInt(result[1]); j++){
				if(j == 0 || j == Integer.parseInt(result[1])-1 || i == 0|| i == Integer.parseInt(result[0])-1){
					line+= '*';
				}else{
					line += '.';
				}
			}
		System.out.println(line);
		}
	}
}
