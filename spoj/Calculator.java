package spoj;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStreamReader;

//so it turns out this doesn't work for large numbers because of all of the parseInts and Integer.toStrings...
//it also seems like as the numbers get bigger, errors start to appear...



// put stuff for subtraction carry into a function
public class Calculator {
	public static ArrayList<String> products = new ArrayList<String>();
	public static String subSum = "0";
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		String amount2 = in.readLine();
		String finalSum = "0";
		amount = addZero(amount, amount2);
		amount2 = addZero(amount2, amount);
		/*  testing xGreater
		String[] xArray = new String[amount.length()];
		String[] yArray = new String[amount.length()];
		for(int i =0;i<amount.length();i++){
			xArray[i]= amount.substring(i,i+1);
		}
		for(int i =0;i<amount2.length();i++){
			yArray[i]= amount2.substring(i,i+1);
		}
		if(xGreater(xArray, yArray)){
			System.out.println("1 is greater");
		}else{
			System.out.println("1 is less");
		}
		*/
		
		//addZero(amount, amount2);
		//addition(amount, amount2);
		division(amount, amount2);
		//multiplication(amount, amount2);
		//System.out.println(products);
		// Actually Important!!!!!!
		//System.out.println("Products equals: " + products);
		for(int i = 0; i<products.size();i++){
			String iProduct = products.get(i);
			//finalSum = addZero(finalSum, iProduct);
			//iProduct = addZero(iProduct, finalSum);
			finalSum = addition(iProduct, finalSum);
			//			System.out.println("final sum " + i +" equals " + finalSum);
			//			System.out.println("iproduct " + i +" equals " + iProduct);
			//			System.out.println(products);
		}
		System.out.println("final sum equals " + finalSum);



	}

	static String addZero(String x, String y){
		String addZero = "";
		if(y.length() > x.length()){
			for(int i = 0; i< y.length() - x.length(); i++){
				addZero += '0';                                                                                              
			}
			x = addZero + x;
		}
		return x;

	}
	static boolean xGreater(String[] cArray, String[] vArray){
		if(cArray.length == vArray.length){
			for(int i = 0; i<cArray.length; i++){
				if(Integer.parseInt(vArray[i]) > Integer.parseInt(cArray[i])){
					return false;
				} 
				if(Integer.parseInt(vArray[i]) < Integer.parseInt(cArray[i])){
					return true;
				} 
			} 
		}
		return true;
	}
	static void subCarry(String[] xArray, String[] yArray, int index){
		/*for(int j = 1; j < yArray.length; j++){
			if(Integer.parseInt(yArray[yArray.length-j-1]) > 0 ){
				yArray[yArray.length-j-1] = Integer.toString(Integer.parseInt(yArray[yArray.length-j-1])-1);
				//System.out.println(yArray[yArray.length-j-1]);

				yArray[yArray.length-j] = Integer.toString(Integer.parseInt(yArray[yArray.length-j])+10);
				//System.out.println(yArray[yArray.length-j]);

			}
		}*/
		int j= 1;
		while(j <xArray.length &&  Integer.parseInt(yArray[index]) > Integer.parseInt(xArray[index])){
			if(Integer.parseInt(xArray[xArray.length-j-1]) > 0 ){
				xArray[xArray.length-j-1] = Integer.toString(Integer.parseInt(xArray[xArray.length-j-1])-1);
				//System.out.println(yArray[yArray.length-j-1]);

				xArray[xArray.length-j] = Integer.toString(Integer.parseInt(xArray[xArray.length-j])+10);
				//System.out.println(yArray[yArray.length-j]);

			}
			j++;
		}
	}
	static void reverseCarry(String[] oDifference){
		for(int i = oDifference.length-1; i >= 0; i--){
			//System.out.println(i + "    " + oDifference[i]);
			if(Integer.parseInt(oDifference[i])>=10){
				oDifference[i-1] = Integer.toString(Integer.parseInt(oDifference[i-1])+(Integer.parseInt(oDifference[i])/10));
				oDifference[i] = Integer.toString(Integer.parseInt(oDifference[i])%10);
				
				if(Integer.parseInt(oDifference[i-1])>= 10){
					i++;
				}
				System.out.println(oDifference[i]);
			}
		}
	}
	static String division(String x, String y){
		x= addZero(x, y);
		y= addZero(y, x);
		String quotient = "";
		boolean isEqual = true;
		String[] xArray = new String[x.length()];
		String[] yArray = new String[x.length()];
		for(int i =0;i<x.length();i++){
			xArray[i]= x.substring(i,i+1);
		}
		for(int i =0;i<y.length();i++){
			yArray[i]= y.substring(i,i+1);
		}
		for(int i = 0; i<xArray.length; i++){
			if(Integer.parseInt(xArray[i])!= Integer.parseInt(yArray[i])){
				isEqual= false;
			}
		}
		if(isEqual == true){
			quotient = "1";
		}
		else if(xGreater(yArray, xArray)){
			quotient = "not divisible";
		}
		else if(isEqual == false && xGreater(xArray, yArray)){
			for(int i = xArray.length-1; i >= 0; i--){
				int count = 0; 
				if(i>0 && Integer.parseInt(yArray[yArray.length-1])>Integer.parseInt(xArray[i])){
					while( Integer.parseInt(yArray[yArray.length-1])*(count) < Integer.parseInt(xArray[i-1]+xArray[i])){
						count++;
					}
					if(Integer.parseInt(yArray[yArray.length-1])*(count)>Integer.parseInt(xArray[i-1]+xArray[i])){
						count--;
					}
				}
				else if(Integer.parseInt(yArray[yArray.length-1])<=Integer.parseInt(xArray[i])){
					while(Integer.parseInt(yArray[yArray.length-1])*(count) < Integer.parseInt(xArray[i])){
						count++;
					}
					if(Integer.parseInt(yArray[yArray.length-1])*(count)>Integer.parseInt(xArray[i])){
						count--;
					}
				}
				quotient += Integer.toString(count);
			}
		}

		
		System.out.println(quotient);
		return quotient;
	}
	static String subtraction(String x, String y){
		ArrayList <String> difference = new ArrayList<String>();
		String sDifference = "";
		String miniDiff = "";
		x= addZero(x, y);
		y= addZero(y, x);
		String[] xArray = new String[x.length()];
		String[] yArray = new String[x.length()];

		for(int i =0;i<x.length();i++){
			xArray[i]= x.substring(i,i+1);
		}
		for(int i =0;i<y.length();i++){
			yArray[i]= y.substring(i,i+1);
		}
		if(xGreater(xArray, yArray)){
			//boolean firstWhile = false;
			for(int i= x.length()-1; i>= 0; i--){
				if(i > 0 && Integer.parseInt(yArray[i]) > Integer.parseInt(xArray[i])){
					while(Integer.parseInt(yArray[i]) > Integer.parseInt(xArray[i])){
							subCarry(xArray, yArray, i);
					}
					
				}
			
				if(Integer.parseInt(xArray[i]) >= Integer.parseInt(yArray[i])){
					miniDiff = Integer.toString(Integer.parseInt(xArray[i])-Integer.parseInt(yArray[i]));
				}
				difference.add(miniDiff); //now must convert difference into a string the other way around
			}
		}else{ 
			for(int i= y.length()-1; i>= 0; i--){
				if(i > 0 && Integer.parseInt(xArray[i]) > Integer.parseInt(yArray[i])){
					while(Integer.parseInt(xArray[i]) > Integer.parseInt(yArray[i])){
							subCarry(yArray, xArray, i);
					}
					
				}
			
				if(Integer.parseInt(yArray[i]) >= Integer.parseInt(xArray[i])){
					miniDiff = Integer.toString(Integer.parseInt(yArray[i])-Integer.parseInt(xArray[i]));
				}
				difference.add(miniDiff); //now must convert difference into a string the other way around
			}
		}
		String[] oDifference = new String[difference.size()];
		int count = 0;
		System.out.println(difference);
		for(int i = difference.size()-1; i >= 0; i--){
			//System.out.println("count = " + count + "diff[i] equals " + difference.get(i));
			if(difference.get(i) != ""){
			oDifference[count] = difference.get(i);
			}
			count++;
		}
		//System.out.println(difference);
		reverseCarry(oDifference);
	
		for(int i = 0; i < oDifference.length; i++){
//			System.out.println("triggered");
//			System.out.println(oDifference[i]);
			sDifference = sDifference+ oDifference[i];
		}
		if(xGreater(xArray, yArray)){ 
			System.out.println("Diff = " + sDifference);
		}else{
			System.out.println("Diff = " + '-'+ sDifference);
		}
		//System.out.println(difference); 
		return sDifference;
	}
	static String addition(String x, String y){ 

		int carryOver=0;
		String sum = "";
		x= addZero(x, y);
		y= addZero(y, x);
		for(int i=x.length()-1; i>= 0; i--){
			int miniSum = Integer.parseInt(x.substring(i, i+1)) + Integer.parseInt(y.substring(i, i+1)) + (carryOver);

			//sum=  Integer.toString(Integer.parseInt(x.substring(i, i+1)) + Integer.parseInt(y.substring(i, i+1))+  (carryOver%10)) + sum;
			sum=  Integer.toString((miniSum)%10) + sum;//some combo of these

			//			System.out.println("x = " + x);
			//			System.out.println("y = " + y);
			//sums.add(sum);

			carryOver = (miniSum)/10;



			if(i == 0 && miniSum >=10){
				//System.out.println("is running");
				sum = Integer.toString((miniSum)/10 ) + sum;
			}
			//			System.out.println("sum is equal to " +sum);
		}
		//System.out.println("the final sum is equal to " +sum);
		//		System.out.println("carryOver = " +carryOver%10);

		return sum;

	}
	static boolean checkZero(String x){
		for(int i = 0; i < x.length(); i++){
			if(x.charAt(i)!= '0'){
				return false;
			}

		}
		return true;
	}
	static void multiplication(String x, String y){//works for things like 1 * 10 but not the other way around also works for single digit multiplication
		String product = "";
		String initialZero = "";
		String actualInitialZero = "";
		for(int i = y.length()-1; i >= 0; i--){
			ArrayList<String> subProducts = new ArrayList<String>();

			for(int j = x.length()-1; j >= 0; j--){
				product = initialZero + actualInitialZero;
				/*	for(int z = 0; z < x.length()-j-1; z++){//tried to replace this with initalZero
					product+= '0';
				}*/
				product = Integer.toString(Integer.parseInt(x.substring(j, j+1)) * Integer.parseInt(y.substring(i, i+1))) + product; //pretty sure this doesnt need carryover
				//System.out.println(product);
				//if(Integer.parseInt(product) != 0){

				if(checkZero(product) == false){ //seems glitched
					subProducts.add(product);
					//System.out.println(subProducts);
				}

				initialZero+='0';



			}
			initialZero = "";
			//System.out.println("subProducts equals: " +subProducts);
			for(int c = 0; c< subProducts.size(); c++){

				String cProduct = subProducts.get(c);
				//subSum = addZero(subSum, cProduct);
				//cProduct = addZero(cProduct, subSum);
				subSum = addition(cProduct, subSum); //cant handle large numbers
		
			}                                   
			products.add(subSum);
			product = ""; 
			subSum = "0";
			//for(int j = 0; j < y.length() -i; j++){
			actualInitialZero += '0';
			//}

		}
	}                                                                                                                

}
