package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class substringCheck {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i<Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem(BufferedReader in) throws IOException {
		String line;
		boolean isOne = false;
		line = in.readLine();
		String[] result = line.split(" ");
		for(int i = 0; i < 5; i++){
			if(result[0].substring(i, i+5).equals(result[1])){
				isOne = true;
			}
		}
		if(isOne){
			System.out.println('1');
		}else{
			System.out.println('0');
		}
	
	}
	
}
