package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class funWithSequences3 {
	public static void main(String[] args) throws IOException {
		ArrayList<Integer> sOne = new ArrayList<Integer>();
		ArrayList<Integer> sTwo = new ArrayList<Integer>();
		String sFinal = new String();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		String line;
		line = in.readLine();
		String[] result = line.split(" ");
		for(int i = 0; i< Integer.parseInt(amount); i++){
			sOne.add(Integer.parseInt(result[i]));
		}
		String amountT = in.readLine();
		String lineT;
		lineT = in.readLine();
		String[] resultT = lineT.split(" ");
		for(int i = 0; i< Integer.parseInt(amountT); i++){
			sTwo.add(Integer.parseInt(resultT[i]));
		}
		for(int i= 0; i< sOne.size() && i< sTwo.size();i++){
			if(i <= sTwo.size()){
				if(sOne.get(i)==sTwo.get(i)){
					sFinal += Integer.toString(i+1) + " ";
				}
		}
		}
		if(sFinal.length()> 0){
			sFinal = sFinal.substring(0, sFinal.length()-1);
			}else{
				sFinal ="";
			}
		System.out.println(sFinal);
	}
}
