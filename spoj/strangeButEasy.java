package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class strangeButEasy{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		//String answer = "";
		for(int i = 0; i<Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem(BufferedReader in) throws IOException {
		ArrayList<Integer> Primes = new ArrayList<>();
		String line;
		int result;
		String finalS = "";
		line = in.readLine();
		for(int i = 0; i<10000; i++){
			if(isPrime(i)){
				Primes.add(i);
			}
		}for(int i =0; i< Integer.parseInt(line)*3; i+=3){
	
		    result = Primes.get(i)*Primes.get(i+1)+Primes.get(i+2);
		    finalS += Integer.toString(result) + " ";
		}
		if(finalS.length()> 0){
			finalS = finalS.substring(0, finalS.length()-1);
			}else{
				finalS ="";
			}
		System.out.println(finalS);
	}
	static boolean isPrime(int x){
		if(x < 2){
			return false;
		}
		for(int i = 2; i <= Math.sqrt(x); i++){
			if(x%i == 0){
				return false;
			}
		}
		return true;
	}
}
