package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class addingReversedNumbers{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem( BufferedReader in) throws IOException{
		String line;
		line = in.readLine();
		String[] result = line.split(" ");
		int sum=0;
		String numOne = new String();
		String numTwo = new String();
		String answer = new String();

		for(int i = result[0].length()-1; i >= 0; i--){
			numOne += result[0].charAt(i);
		}
		for(int i = result[1].length()-1; i >= 0; i--){
			numTwo += result[1].charAt(i);
		}

		sum = Integer.parseInt(numOne)+Integer.parseInt(numTwo); 
		for(int i = Integer.toString(sum).length()-1; i >= 0; i--){
			answer += Integer.toString(sum).charAt(i);
		}
		if(answer.length()> 1){
			for(int i = 0; i< answer.length(); i++){
				if(answer.charAt(i)=='0'){
					answer=answer.substring(i+1, answer.length());
				}
			}
		}
		
		System.out.println(Integer.parseInt(answer));
	}
}