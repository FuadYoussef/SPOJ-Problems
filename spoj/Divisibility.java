package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;




//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");

//work but not working on spoj

public class Divisibility{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i<Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem(BufferedReader in) throws IOException {
		String Answers = "";
		//ArrayList<Integer> Answers = new ArrayList<>();
		int n = 0;
		int x = 0;
		int y = 0;
		String line;
		line = in.readLine();
		String[] result = line.split(" ");
        	n = Integer.parseInt(result[0]);
        	x = Integer.parseInt(result[1]);
        	y = Integer.parseInt(result[2]);
     
        for(int i = 2; i< n;i++){
        	if(i%x == 0 && i%y != 0){
        		Answers+= Integer.toString(i)+" ";
        	}
        }
        System.out.println(Answers);
	}
}
