package spoj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class fastMultiplication {
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doOneProblem( BufferedReader in) throws IOException{
		String line;
		line = in.readLine();
		String[] result = line.split(" ");
		int answer=0;
		if(result.length == 2){
			answer = Integer.parseInt(result[0])*Integer.parseInt(result[1]); //the error seems to be with this line
		}
		System.out.println(Integer.toString(answer));
	}
}
