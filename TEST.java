import java.io.*;

//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class TEST {

	public static void main(String[] args) throws IOException {
				
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		while (true) {
			line = in.readLine();
			if (line == null || line.equals("42")) {
				break;
			}
			System.out.println(line);
		}
	}
}
