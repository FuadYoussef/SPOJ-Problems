import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class Wow{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		String solution = "W";
		for(int i = 0; i<Integer.parseInt(amount); i++){
			solution += "o";
		}
		solution += "w";
		System.out.println(solution);
	}
}