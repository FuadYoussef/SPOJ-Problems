import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");



public class characterPatternsThree{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	static void doFirstGrid() throws IOException {
		System.out.println("****");
		System.out.println("*..*");
		System.out.println("*..*");
		System.out.println("****");
	}
	static void doOneGrid() throws IOException {
		System.out.println("*..*");
		System.out.println("*..*");
		System.out.println("****");	
	}
	static void doSideFirstGrid() throws IOException {
		System.out.println("***");
		System.out.println("..*");
		System.out.println("..*");
		System.out.println("***");
	}
	static void doSideGrid() throws IOException {
		System.out.println("..*");
		System.out.println("..*");
		System.out.println("***");
	}
	
	static void doOneProblem(BufferedReader in) throws IOException {
		String input;
		input = in.readLine();
		String[] result = input.split(" ");
		if(Integer.parseInt(result[0])> 0){
		doFirstGrid();
		}
		for(int i = 1; i < Integer.parseInt(result[0]); i++){
			doOneGrid();
		}
		if(Integer.parseInt(result[1]) > 1){
			doSideFirstGrid();
			for(int j = 1; j < Integer.parseInt(result[1]); j++){
				doSideGrid();
			}
		}
		
	}
}