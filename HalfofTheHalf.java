import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//http://www.spoj.com/problems/basics/
// Integer.parseInt("23");




public class HalfofTheHalf{
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String amount = in.readLine();
		for(int i = 0; i < Integer.parseInt(amount); i++){
			doOneProblem(in);
		}
	}
	
	static void doOneProblem(BufferedReader in) throws IOException {
		String result = "";
		String line;
		line = in.readLine();
		for(int i = 0; i < line.length()/2; i++){
			if(i%2 == 0){
			result+= line.charAt(i);
			}
		}
		System.out.println(result);
		
	}
}